import React, { Component } from 'react';
import ReactDOM from 'react-dom';

export default class Main extends Component {
    render() {
        return (
            <div className="main-container">
                <h1>Zdarova</h1>    
            </div>
        )
    }
}
ReactDOM.render(<Main />, document.getElementById('root'))
